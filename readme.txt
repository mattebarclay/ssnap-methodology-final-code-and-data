This repository contains the analysis code used for Chapter 4 of the thesis.

This top-level contains the different analysis files, each run in Stata v15 for my analysis.

The folder 'data' contains the raw data downloaded from the SSNAP website, and then the various datasets produced by my analysis.

The folder 'results' contains the different (Stata) figures used in the thesis, plus some excel files of raw results used to complete tables. Other results are not in there, because they were read from Stata's results window. This is bad form but was convenient.

Details of the analysis code files in order that they should be run to reproduce the results in Chapter 4 of the thesis.

The first analysis code file is SSNAP0_LoadDataSSNAP.do. This file loads in the raw data from the July-September Excel file and converts it into something that is easier to handle in Stata.

The second file is SSNAP1_Standardise.do. This file applies the current SSNAP and my plausible alternative standardisation approaches to the raw data. It also exports a table showing the mean hospital-level performance on each of the measures under the different standardisation approaches, and without standardisation.

The third file, SSNAP2_EFA.do, applies exploratory factor analysis to look for latent factors within the performance measures to identify possible empirical domains of quality. This file is on its own because it takes a while to run, so it is frustrating if accidentally run when it does not need to be run.

The fourth file, SSNAP3_Group.do, groups the measures according to the exploratory factor analysis results, and to the current domains used in SSNAP.

The fifth file, SSNAP4_Combine.do, calculates the domain scores and the overall summary scores for the SSNAP with each of the permutations considered in this anaylsis.

The sixth file, SSNAP5_Main_plots_and_results.do, produces the main set of results for this analysis. This does not really do any analysis as such, it simply produces plots and tables based on the analysis done in previous files.

The seventh file, SSNAP6_MonteCarloWeights.do, runs the four Monte Carlo simulations showing the impact of plausible domain weights.

The eigth file, SSNAP7_individual_domain_scores.do, produces a single additional figure showing correlations between hospital domain scores under different approaches to standardisation.

