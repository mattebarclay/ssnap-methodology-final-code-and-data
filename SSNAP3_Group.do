/******************************************************************************/
/* 	Group SSNAP data both into key indicator domains and into 
	the domains suggested by factor analysis
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"


/******************************************************************************/
/* Load data																  */
use data/SSNAP_std.dta, clear

/******************************************************************************/
/* Work with SSNAP-standardised scores										  */

* factor 1 - good care
foreach var in 	d1_m1_std1  /// /* scanned within 1 hour */
				d1_m2_std1  /// /* % patients scanned within 12 hours */
 				d3_m2_std1  /// /* % eligible patients given thrombolysis */
				d4_m1_std1  /// /* % patients assessed by a stroke specialist within 24 hours */
				d4_m3_std1  /// /* % patients assessed by a stroke nurse within 24 hours */
				d4_m5_std1  /// /* % applicable patients given a swallow screen within 24 hours */
				d4_m6_std1  /// /* % applicable patients given a formal swallow assessment */
				d8_m1_std1  /// /* % applicable patients assessed by occupational therapist */
				d8_m2_std1  /// /* Median time until assessed by occupational therapist */
				d8_m3_std1  /// /* % applicable patients assessed by a physiotherapist within */
				d8_m5_std1  /// /* % applicable patients assessed by a speech therapist within */
				d8_m7_std1  /// /* % applicable patients with rehab goals agreed within 5 days */
				d8_m8_std1  /// /* % applicable patients assessed by all relevant specialists */
				d9_m1_std1  /// /* % applicable patients screened for nutrition and seen by dietitian */
				d9_m2_std1  /// /* % applicable patients with a continence plan drawn up within */
				d9_m3_std1  /// /* % applicable patients who have mood and cognition screening */
				d10_m4_std1 /// /* % patients discharged alive who are given a named person to */
				{ 
	gen efa1_1_`var' = `var' 
} 

* factor 2 - stroke unit
foreach var in 	d1_m3_std1  /// /* Median time until scanned */
				d2_m1_std1  /// /* % patients directly admitted within 4 hours */
				d2_m2_std1  /// /* Median time until arrival on stroke unit */
				d2_m3_std1  /// /* % patients spending at least 90% of stay on a stroke unit */
				d3_m1_std1  /// /* % all stroke patients given thrombolysis */
				d3_m4_std1  /// /* % applicable patients admitted within 4 hrs AND get */
				d4_m2_std1  /// /* Median time until assessed by a stroke specialist */
				d4_m4_std1  /// /* Median time until assessed by a stroke nurse */
				{
	gen efa1_2_`var' = `var'
}

* factor 3 - receipt of occupational and physiotherapy
foreach var in 	d5_m3_std1  /// /* Median % days on which occupational therapy is received */
				d5_m4_std1  /// /* % compliance against therapy target for occupational therapy */
				d6_m3_std1  /// /* Median % days on which physiotherapy is received */
				d6_m4_std1  /// /* % compliance against therapy target for physiotherapy */
				d8_m4_std1  /// /* Median time until assessed by physiotherapist */
				{
	gen efa1_3_`var' = `var'
}

* factor 4 - identification of therapy need
foreach var in 	d5_m1_std1 		/// /* % patients reported as requiring occupational therapy */
				d6_m1_std1 		/// /* % patients reported as requiring physiotherapy */
				d7_m1_std1 		/// /* % patients reported as requiring speech therapy */
				d10_m1_std1 	/// /* % applicable patients receiving a joint health and social */
				d10_m2_std1 	/// /* % patients treated by a stroke-skilled Early Supported */
				d10_m3_std1 	/// /* % applicable patients in atrial fibrillation discharged on */
				{
	gen efa1_4_`var' = `var'
}

* factor 5 - rapid thrombolysis
foreach var in 	d3_m3_std1  /// /* % patients thrombolysed within 1 hour */
				d3_m5_std1  /// /* Median time until thrombolysis */
				{
	gen efa1_5_`var' = `var'
}

* factor 6 - receipt of speech and language therapy
foreach var in 	d7_m3_std1 /// /* Median % days on which speech therapy is received */
				d7_m4_std1 /// /* % compliance against therapy target for speech therapy */
				d8_m6_std1 /// /* Median time until assessed by speech therapist */
				{
	gen efa1_6_`var' = `var'
}

* factor 7 - time receiving therapy per day
foreach var in 	d5_m2_std1 /// /* Median minutes per day receiving occupational therapy */
				d6_m2_std1 /// /* Median minutes per day receiving physiotherapy */
				d7_m2_std1 /// /* Median minutes per day receiving speech therapy */
				{
	gen efa1_7_`var' = `var'
}

/******************************************************************************/
/* Work with Z-standardised scores											  */
desc d?_m? d??_m?

* factor 1 - good care
foreach var in 	d1_m1_std2  /// /* scanned within 1 hour */
				d1_m2_std2  /// /* % patients scanned within 12 hours */
 				d3_m2_std2  /// /* % eligible patients given thrombolysis */
				d4_m1_std2  /// /* % patients assessed by a stroke specialist within 24 hours */
				d4_m3_std2  /// /* % patients assessed by a stroke nurse within 24 hours */
				d4_m5_std2  /// /* % applicable patients given a swallow screen within 24 hours */
				d4_m6_std2  /// /* % applicable patients given a formal swallow assessment */
				d8_m1_std2  /// /* % applicable patients assessed by occupational therapist */
				d8_m2_std2  /// /* Median time until assessed by occupational therapist */
				d8_m3_std2  /// /* % applicable patients assessed by a physiotherapist within */
				d8_m5_std2  /// /* % applicable patients assessed by a speech therapist within */
				d8_m7_std2  /// /* % applicable patients with rehab goals agreed within 5 days */
				d8_m8_std2  /// /* % applicable patients assessed by all relevant specialists */
				d9_m1_std2  /// /* % applicable patients screened for nutrition and seen by dietitian */
				d9_m2_std2  /// /* % applicable patients with a continence plan drawn up within */
				d9_m3_std2  /// /* % applicable patients who have mood and cognition screening */
				d10_m4_std2 /// /* % patients discharged alive who are given a named person to */
				{
	gen efa2_1_`var' = `var' 
} 

* factor 2 - stroke unit
foreach var in 	d1_m3_std2  /// /* Median time until scanned */
				d2_m1_std2  /// /* % patients directly admitted within 4 hours */
				d2_m2_std2  /// /* Median time until arrival on stroke unit */
				d2_m3_std2  /// /* % patients spending at least 90% of stay on a stroke unit */
				d3_m1_std2  /// /* % all stroke patients given thrombolysis */
				d3_m4_std2  /// /* % applicable patients admitted within 4 hrs AND get */
				d4_m2_std2  /// /* Median time until assessed by a stroke specialist */
				d4_m4_std2  /// /* Median time until assessed by a stroke nurse */
				{
	gen efa2_2_`var' = `var' 
} 

* factor 3 - receipt of occupational and physiotherapy
foreach var in 	d5_m3_std2  /// /* Median % days on which occupational therapy is received */
				d5_m4_std2  /// /* % compliance against therapy target for occupational therapy */
				d6_m3_std2  /// /* Median % days on which physiotherapy is received */
				d6_m4_std2  /// /* % compliance against therapy target for physiotherapy */
				d8_m4_std2  /// /* Median time until assessed by physiotherapist */
				{
	gen efa2_3_`var' = `var' 
} 

* factor 4 - identification of therapy need
foreach var in 	d5_m1_std2 		/// /* % patients reported as requiring occupational therapy */
				d6_m1_std2 		/// /* % patients reported as requiring physiotherapy */
				d7_m1_std2 		/// /* % patients reported as requiring speech therapy */
				d10_m1_std2 	/// /* % applicable patients receiving a joint health and social */
				d10_m2_std2 	/// /* % patients treated by a stroke-skilled Early Supported */
				d10_m3_std2 	/// /* % applicable patients in atrial fibrillation discharged on */
				{
	gen efa2_4_`var' = `var' 
} 

* factor 5 - rapid thrombolysis
foreach var in 	d3_m3_std2  /// /* % patients thrombolysed within 1 hour */
				d3_m5_std2  /// /* Median time until thrombolysis */
				{
	gen efa2_5_`var' = `var' 
} 

* factor 6 - receipt of speech and language therapy
foreach var in 	d7_m3_std2 /// /* Median % days on which speech therapy is received */
				d7_m4_std2 /// /* % compliance against therapy target for speech therapy */
				d8_m6_std2 /// /* Median time until assessed by speech therapist */
				{
	gen efa2_6_`var' = `var' 
} 

* factor 7 - time receiving therapy per day
foreach var in 	d5_m2_std2 /// /* Median minutes per day receiving occupational therapy */
				d6_m2_std2 /// /* Median minutes per day receiving physiotherapy */
				d7_m2_std2 /// /* Median minutes per day receiving speech therapy */
				{
	gen efa2_7_`var' = `var' 
} 

/******************************************************************************/
/* Save																		  */
save data/SSNAP_grouped.dta, replace
