/******************************************************************************/
/* 	Group SSNAP data - do the exploratory factor analysis
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"

/******************************************************************************/
/* Load data																  */
use data/SSNAP_std.dta, clear

/******************************************************************************/
/* Work with SSNAP-standardised scores										  */
use data/SSNAP_std.dta, clear

* Audit approach
factor *_std1, ml
screeplot, yline(1)
* elbows at 4, 7 factors
factor *_std1, ml factors(7)
rotate, promax

* Z-scores
factor *_std2, ml
screeplot, yline(1)
* elbow at 8?
factor *_std1, ml factors(8)
rotate, promax


