/******************************************************************************/
/* 	Standardise SSNAP data
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"


/******************************************************************************/
/* Load data																  */
use data/SSNAP_loaded.dta, clear

* exclude hospitals with no data
egen rowmis = rowmiss(d*)
tab rowmis

list hospital rowmis if rowmis >= 1
drop if rowmis == 44 // Leeds General Infirmary and Downe General Hospital - all missing

list hospital d* if rowmis == 3
// Southport - missing d3_m2 d3_m3 d3_m5


/******************************************************************************/
/* Standardise - SSNAP approach												  */

* Domain 1 - scanning
local std d1_m1_std1
local raw d1_m1
gen `std' = min(100,2*`raw') if !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d1_m2_std1
local raw d1_m2
gen `std' = .
replace `std' = 100 if `raw' > 95 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' > 90 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' > 85 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' > 80 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' > 75 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' > 70 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' > 65 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' > 60 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' > 55 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' > 50 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d1_m3_std1
local raw d1_m3
gen `std' = .
replace `std' = 100 if `raw' <  45/60 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' <  60/60 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' <  75/60 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' <  90/60 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 120/60 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 180/60 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 240/60 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 300/60 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 360/60 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 480/60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 2 - Stroke unit admission
gen     d2_m1_std1 = d2_m1
scatter d2_m1_std1   d2_m1, msymb(oh) mcolor(blue%20) jitter(5)

local std d2_m2_std1
local raw d2_m2
gen `std' = .
replace `std' = 100 if `raw' <  60/60 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' < 120/60 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' < 180/60 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' < 240/60 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 270/60 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 300/60 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 330/60 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 360/60 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 420/60 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 480/60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

gen     d2_m3_std1 = d2_m3
scatter d2_m3_std1   d2_m3, msymb(oh) mcolor(blue%20) jitter(5)

* Domain 3 - thrombolysis
local std d3_m1_std1
local raw d3_m1
gen `std' = min(100,5*`raw') if !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d3_m2_std1
local raw d3_m2
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d3_m3_std1
local raw d3_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d3_m4_std1
local raw d3_m4
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d3_m5_std1
local raw d3_m5
gen `std' = .
replace `std' = 100 if `raw' <  30/60 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' <  40/60 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' <  50/60 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' <  60/60 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' <  70/60 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' <  80/60 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' <  90/60 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 100/60 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 110/60 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 120/60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 4 - specialist assessment
local std d4_m1_std1
local raw d4_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d4_m2_std1
local raw d4_m2
gen `std' = .
replace `std' = 100 if `raw' <  3 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' <  6 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' <  9 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' < 12 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 15 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 18 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 21 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 24 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 36 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 48 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d4_m3_std1
local raw d4_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d4_m4_std1
local raw d4_m4
gen `std' = .
replace `std' = 100 if `raw' < 30/60 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' <  1 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' <  2 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' <  3 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' <  6 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' <  9 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 12 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 15 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 18 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 21 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d4_m5_std1
local raw d4_m5
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d4_m6_std1
local raw d4_m6
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 5 - occupational therapy
local std d5_m1_std1
local raw d5_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d5_m2_std1
local raw d5_m2
gen `std' = .
replace `std' = 100 if `raw' >  40 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' >  32 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' >  28 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' >  24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' >  20 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' >  16 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' >  12 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' >   8 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' >   4 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' >   0 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d5_m3_std1
local raw d5_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* d5_m4 - can score above 100% !?
local std d5_m4_std1
local raw d5_m4
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 6 - physiotherapy
local std d6_m1_std1
local raw d6_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d6_m2_std1
local raw d6_m2
gen `std' = .
replace `std' = 100 if `raw' >  40 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' >  32 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' >  28 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' >  24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' >  20 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' >  16 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' >  12 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' >   8 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' >   4 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' >   0 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d6_m3_std1
local raw d6_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* d6_m4 - can score above 100% !?
local std d6_m4_std1
local raw d6_m4
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 7 - speech and language therapy
local std d7_m1_std1
local raw d7_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d7_m2_std1
local raw d7_m2
gen `std' = .
replace `std' = 100 if `raw' >  40 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' >  32 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' >  28 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' >  24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' >  20 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' >  16 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' >  12 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' >   8 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' >   4 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' >   0 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d7_m3_std1
local raw d7_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* d7_m4 - can score above 100% !?
local std d7_m4_std1
local raw d7_m4
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 8 - combined therapy
local std d8_m1_std1
local raw d8_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m2_std1
local raw d8_m2
gen `std' = .
replace `std' = 100 if `raw' <  6 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' < 12 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' < 18 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' < 24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 30 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 36 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 42 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 48 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 54 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m3_std1
local raw d8_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m4_std1
local raw d8_m4
gen `std' = .
replace `std' = 100 if `raw' <  6 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' < 12 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' < 18 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' < 24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 30 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 36 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 42 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 48 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 54 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m5_std1
local raw d8_m5
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m6_std1
local raw d8_m6
gen `std' = .
replace `std' = 100 if `raw' <  6 & missing(`std') & !missing(`raw')
replace `std' =  90 if `raw' < 12 & missing(`std') & !missing(`raw')
replace `std' =  80 if `raw' < 18 & missing(`std') & !missing(`raw')
replace `std' =  70 if `raw' < 24 & missing(`std') & !missing(`raw')
replace `std' =  60 if `raw' < 30 & missing(`std') & !missing(`raw')
replace `std' =  50 if `raw' < 36 & missing(`std') & !missing(`raw')
replace `std' =  40 if `raw' < 42 & missing(`std') & !missing(`raw')
replace `std' =  30 if `raw' < 48 & missing(`std') & !missing(`raw')
replace `std' =  20 if `raw' < 54 & missing(`std') & !missing(`raw')
replace `std' =  10 if `raw' < 60 & missing(`std') & !missing(`raw')
replace `std' =   0 if              missing(`std') & !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m7_std1
local raw d8_m7
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d8_m8_std1
local raw d8_m8
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 9 - discharge
local std d9_m1_std1
local raw d9_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d9_m2_std1
local raw d9_m2
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d9_m3_std1
local raw d9_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

* Domain 10 - joint working
local std d10_m1_std1
local raw d10_m1
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d10_m2_std1
local raw d10_m2
gen `std' = min(100,2.5*`raw') if !missing(`raw')
assert !missing(`std') if !missing(`raw')
scatter `std' `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d10_m3_std1
local raw d10_m3
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

local std d10_m4_std1
local raw d10_m4
gen     `std' = `raw'
scatter `std'   `raw', msymb(oh) mcolor(blue%20) jitter(5)

/******************************************************************************/
/* Standardise - Z-score approach											  */
local max = 0
local min = 0
forval domain = 1/10 {
	forval measure = 1/8 {
		cap confirm numeric variable d`domain'_m`measure'
		if _rc {
			// skip
		}
		qui else {
			summ d`domain'_m`measure'
			local sd = r(sd)
			local mean = r(mean)
			gen d`domain'_m`measure'_std2 = (d`domain'_m`measure'-`mean')/`sd'
			
			summ d`domain'_m`measure'_std2
			local max = max(`max',r(max))
			local min = min(`min',r(min))
		}
	}
}

di "Max: `max'"
di "Min: `min'"

scatter d1_m1_std2 d1_m1_std1

/******************************************************************************/
/* Save																		  */
compress
save data/SSNAP_std.dta, replace

/******************************************************************************/
/* Table of measure performance												  */
use data/SSNAP_std.dta, replace
rename (d*) =_
rename (*std*_) (*std*)
rename (d*_m*_) (d*_m*_std0)
rename (d*_m*_std0 d*_m*_std1 d*_m*_std2) (std0_d*_m* std1_d*_m* std2_d*_m*)

qui forval d = 1/10 {
	forval m = 1/8 {
		cap local labl : variable label std0_d`d'_m`m'
		if _rc {
			// if variable does not exist, skip
		}
		else {
			gen label_d`d'_m`m' = "`labl'"
		}
	}
}

reshape long std0_ std1_ std2_ label_, i(hospital) j(domain) string
rename (*_) (*)

split domain, parse("_") gen(d)
gen byte d = real(substr(d1,2,.))
gen byte m = real(substr(d2,2,.))
drop d1 d2

gen d_str = ""
replace d_str = "Scanning" if d == 1
replace d_str = "Stroke unit" if d == 2
replace d_str = "Thrombolysis" if d == 3
replace d_str = "Specialist assessments" if d == 4
replace d_str = "Occupational therapy" if d == 5
replace d_str = "Physiotherapy" if d == 6
replace d_str = "Speech and language therapy" if d == 7
replace d_str = "MDT working" if d == 8
replace d_str = "Standards by discharge" if d == 9
replace d_str = "Discharge processes" if d == 10

order d m d_str std0 std1 std2

preserve

#delimit ;
collapse 	(mean) 	std0_mean 	= std0
					std1_mean 	= std1
					std2_mean 	= std2
			(sd)	std0_sd		= std0
					std1_sd 	= std1
					std2_sd		= std2
			,	by(d_str d m label)
			;
#delimit cr
sort d m
order d m d_str label std0_m std0_sd std1_m std1_sd std2_m std2_sd
list in 1/10

export excel "results/ssnap_domain_scores.xlsx", replace firstrow(var)

restore

list if hospital == "AddenbrookesHospital" & domain == "d6_m2"
sort domain std2
by domain: gen ranking = _n
list if domain == "d6_m2" & ranking == 1

desc
collapse (mean) std1 std2, by(hospital d)
sort d std2
by d: gen ranking = _n
list if inlist(ranking, 1, 2) & inlist(d, 9, 10)

