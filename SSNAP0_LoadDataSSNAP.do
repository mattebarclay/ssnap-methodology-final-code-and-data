/******************************************************************************/
/* 	LOAD DATA SSNAP

	Loads SSNAP data from spreadsheets into analytically tractable information.
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"


/******************************************************************************/
/* Load data																  */
import excel using "data/JulSep2019-FullResultsPortfolio.xls" /// 
	, 	sheet("A. Key Indicators") ///
		cellrange("A4:EM154") ///
		firstrow

rename Theitemreferencesshowwheret measure

* remove "team-centred"
drop if (substr(measure,4,1) == "B" | substr(measure,5,1) == "B") 

* get measure name
replace measure = measure[_n-1] if (substr(measure,4,1) == "A" | substr(measure,5,1) == "A") 
drop if measure[_n+1] == measure[_n]

* get rid of summary rows
replace measure = trim(subinstr(measure, "See technical information for how calculated", "", .))
replace measure = trim(subinstr(measure, char(10), "", .))
replace measure = trim(subinstr(measure, char(13), "", .))
#delimit ;
drop if inlist(measure	, "Number of patients (72h cohort):"
						, "Patient-centred 72h cohort (D2.2)"
						, "Number of patients (post-72h cohort):"
						, "Patient-centred post-72h cohort (D5.2)"
						, "1. Scanning key indicators"
						, "2. Stroke unit key indicators"
						, "3. Thrombolysis key indicators"
				);
drop if inlist(measure	, "4. Specialist assessments key indicators"
						, "5. Occupational therapy key indicators"
						, "6. Physiotherapy key indicators"
						, "7. Speech and language therapy key indicators"
						, "8. Multidisciplinary team working key indicators"
						, "9. Standards by discharge key indicators"
						, "10. Discharge processes key indicators"
				);
drop if inlist(measure	, "Team-centred 72h cohort (D1.4)"
						, "Team-centred post-72h cohort (7 day team) (D6.1)"
						, "Team-centred post-72h cohort (inpatient discharging team) (D3.1)"
						, "Team-centred post-72h cohort (all teams) (D4.3)"
				);
#delimit cr

* update names to something manageable (for measure names...)
replace measure = "d1_m1" if measure == "1.1 Percentage of patients scanned within 1 hour of clock start"
replace measure = "d1_m2" if measure == "1.2 Percentage of patients scanned within 12 hours of clock start"
replace measure = "d1_m3" if measure == "1.3 Median time between clock start and scan (hours:mins)"

replace measure = "d2_m1" if measure == "2.1 Percentage of patients directly admitted to a stroke unit within 4 hours of clock start*"
replace measure = "d2_m2" if measure == "2.2 Median time between clock start and arrival on stroke unit (hours:mins)"
replace measure = "d2_m3" if measure == "2.3 Percentage of patients who spent at least 90% of their stay on stroke unit"

replace measure = "d3_m1" if measure == "3.1 Percentage of all stroke patients given thrombolysis (all stroke types)"
replace measure = "d3_m2" if measure == "3.2 Percentage of eligible patients (according to the RCP guideline minimum threshold) given thrombolysis"
replace measure = "d3_m3" if measure == "3.3 Percentage of patients who were thrombolysed within 1 hour of clock start"
replace measure = "d3_m4" if measure == "3.4 Percentage of applicable patients directly admitted to a stroke unit within 4 hours of clock start AND who either receive thrombolysis or have a pre-specified justifiable reason ('no but') for why it could not be given*"
replace measure = "d3_m5" if measure == "3.5 Median time between clock start and thrombolysis (hours:mins)"

replace measure = "d4_m1" if measure == "4.1 Percentage of patients assessed by a stroke specialist consultant physician within 24h of clock start**"
replace measure = "d4_m2" if measure == "4.2 Median time between clock start and being assessed by stroke consultant (hours:mins)**"
replace measure = "d4_m3" if measure == "4.3 Percentage of patients who were assessed by a nurse trained in stroke management within 24h of clock start"
replace measure = "d4_m4" if measure == "4.4 Median time between clock start and being assessed by stroke nurse (hours:mins)"
replace measure = "d4_m5" if measure == "4.5 Percentage of applicable patients who were given a swallow screen within 4h of clock start"
replace measure = "d4_m6" if measure == "4.6 Percentage of applicable patients who were given a formal swallow assessment within 72h of clock start"

replace measure = "d5_m1" if measure == "5.1 Percentage of patients reported as requiring occupational therapy"
replace measure = "d5_m2" if measure == "5.2 Median number of minutes per day on which occupational therapy is received"
replace measure = "d5_m3" if measure == "5.3 Median % of days as an inpatient on which occupational therapy is received"
replace measure = "d5_m4" if measure == "5.4 Compliance (%) against the therapy target of an average of 25.7 minutes of occupational therapy across all patients (Target = 45 minutes x (5/7) x 0.8 which is 45 minutes of occupational therapy x 5 out of 7 days per week x 80% of patients)"

replace measure = "d6_m1" if measure == "6.1 Percentage of patients reported as requiring physiotherapy"
replace measure = "d6_m2" if measure == "6.2 Median number of minutes per day on which physiotherapy is received"
replace measure = "d6_m3" if measure == "6.3 Median % of days as an inpatient on which physiotherapy is received"
replace measure = "d6_m4" if measure == "6.4 Compliance (%) against the therapy target of an average of 27.3 minutes of physiotherapy across all patients (Target = 45 minutes x (5/7) x 0.85 which is 45 minutes of physiotherapy x 5 out of 7 days per week x 85% of patients)"

replace measure = "d7_m1" if measure == "7.1 Percentage of patients reported as requiring speech and language therapy"
replace measure = "d7_m2" if measure == "7.2 Median number of minutes per day on which speech and language therapy is received"
replace measure = "d7_m3" if measure == "7.3 Median % of days as an inpatient on which speech and language therapy is received"
replace measure = "d7_m4" if measure == "7.4 Compliance (%) against the therapy target of an average of 16.1 minutes of speech and language therapy across all patients (Target = 45 minutes x (5/7) x 0.5 which is 45 minutes of speech and language therapy x 5 out of 7 days per week x 50% of patients)"

replace measure = "d8_m1" if measure == "8.1 Percentage of applicable patients who were assessed by an occupational therapist within 72h of clock start"
replace measure = "d8_m2" if measure == "8.2 Median time between clock start and being assessed by occupational therapist (hours:mins)"
replace measure = "d8_m3" if measure == "8.3 Percentage of applicable patients who were assessed by a physiotherapist within 72h of clock start"
replace measure = "d8_m4" if measure == "8.4 Median time between clock start and being assessed by physiotherapist (hours:mins)"
replace measure = "d8_m5" if measure == "8.5 Percentage of applicable patients who were assessed by a speech and language therapist within 72h of clock start"
replace measure = "d8_m6" if measure == "8.6 Median time between clock start and being assessed by speech and language therapist (hours:mins)"
replace measure = "d8_m7" if measure == "8.7 Percentage of applicable patients who have rehabilitation goals agreed within 5 days of clock start"
replace measure = "d8_m8" if measure == "8.8 Percentage of applicable patients who are assessed by a nurse within 24h AND at least one therapist within 24h AND all relevant therapists within 72h AND have rehab goals agreed within 5 days"

replace measure = "d9_m1" if measure == "9.1 Percentage of applicable patients screened for nutrition and seen by a dietitian by discharge (excluding patients on palliative care)"
replace measure = "d9_m2" if measure == "9.2 Percentage of applicable patients who have a continence plan drawn up within 3 weeks of clock start"
replace measure = "d9_m3" if measure == "9.3 Percentage of applicable patients who have mood and cognition screening by discharge"

replace measure = "d10_m1" if measure == "10.1 Percentage of applicable patients receiving a joint health and social care plan on discharge"
replace measure = "d10_m2" if measure == "10.2 Percentage of patients treated by a stroke skilled Early Supported Discharge team"
replace measure = "d10_m3" if measure == "10.3 Percentage of applicable patients in atrial fibrillation on discharge who are discharged on anticoagulants or with a plan to start anticoagulation"
replace measure = "d10_m4" if measure == "10.4 Percentage of those patients who are discharged alive who are given a named person to contact after discharge"

* get numeric
foreach var of varlist * {
	cap replace `var' = "" if `var' == "N/A"
	cap replace `var' = subinstr(`var',":",".",.)
	cap destring `var', replace
}

* xpose
xpose, clear varname

drop if inlist(_varname, "measure", "B", "C", "D")

* rename as before...
* need to do some work on clock time measures yet!
* so these are marked out as "_c"
rename (v1 v2 v3) (d1_m1 d1_m2 d1_m3_c)
label var d1_m1 "% patients scanned within 1 hour"
label var d1_m2 "% patients scanned within 12 hours"
label var d1_m3_c "Median time until scanned"

rename (v4 v5 v6) (d2_m1 d2_m2 d2_m3_c)
label var d2_m1 "% patients directly admitted within 4 hours"
label var d2_m2 "Median time until arrival on stroke unit"
label var d2_m3_c "% patients spending at least 90% of stay on a stroke unit"

rename (v7 v8 v9 v10 v11) (d3_m1 d3_m2 d3_m3 d3_m4 d3_m5_c)
label var d3_m1 "% all stroke patients given thrombolysis"
label var d3_m2 "% eligible patients given thrombolysis"
label var d3_m3 "% patients thrombolysed within 1 hour"
label var d3_m4 "% applicable patients admitted within 4 hrs AND get thrombolysis"
label var d3_m5_c "Median time until thrombolysis"

rename (v12 v13 v14 v15 v16 v17) (d4_m1 d4_m2_c d4_m3 d4_m4_c d4_m5 d4_m6)
label var d4_m1 "% patients assessed by a stroke specialist within 24 hours"
label var d4_m2_c "Median time until assessed by a stroke specialist"
label var d4_m3 "% patients assessed by a stroke nurse within 24 hours"
label var d4_m4_c "Median time until assessed by a stroke nurse"
label var d4_m5 "% applicable patients given a swallow screen within 24 hours"
label var d4_m6 "% applicable patients given a formal swallow assessment within 72 hours"

rename (v18 v19 v20 v21) (d5_m1 d5_m2 d5_m3 d5_m4)
label var d5_m1 "% patients reported as requiring occupational therapy"
label var d5_m2 "Median minutes per day receiving occupational therapy"
label var d5_m3 "Median % days on which occupational therapy is received"
label var d5_m4 "% compliance against therapy target for occupational therapy"

rename (v22 v23 v24 v25) (d6_m1 d6_m2 d6_m3 d6_m4)
label var d6_m1 "% patients reported as requiring physiotherapy"
label var d6_m2 "Median minutes per day receiving physiotherapy"
label var d6_m3 "Median % days on which physiotherapy is received"
label var d6_m4 "% compliance against therapy target for physiotherapy"

rename (v26 v27 v28 v29) (d7_m1 d7_m2 d7_m3 d7_m4)
label var d7_m1 "% patients reported as requiring speech therapy"
label var d7_m2 "Median minutes per day receiving speech therapy"
label var d7_m3 "Median % days on which speech therapy is received"
label var d7_m4 "% compliance against therapy target for speech therapy"

rename 	(v30 	v31 	v32 	v33 	v34 	v35 	v36 	v37) ///
		(d8_m1 	d8_m2_c	d8_m3 	d8_m4_c	d8_m5	d8_m6_c d8_m7	d8_m8)
label var d8_m1 "% applicable patients assessed by occupational therapist within 72 hours"
label var d8_m2_c "Median time until assessed by occupational therapist"
label var d8_m3 "% applicable patients assessed by a physiotherapist within 72 hours"
label var d8_m4_c "Median time until assessed by physiotherapist"
label var d8_m5 "% applicable patients assessed by a speech therapist within 72 hours"
label var d8_m6_c "Median time until assessed by speech therapist"
label var d8_m7 "% applicable patients with rehab goals agreed within 5 days"
label var d8_m8 "% applicable patients assessed by all relevant specialists in a timely manner"

rename (v38 v39 v40) (d9_m1 d9_m2 d9_m3)
label var d9_m1 "% applicable patients screened for nutrition and seen by dietician by discharge"
label var d9_m2 "% applicable patients with a continence plan drawn up within 3 weeks"
label var d9_m3 "% applicable patients who have mood and cognition screening by discharge"

rename (v41 v42 v43 v44) (d10_m1 d10_m2 d10_m3 d10_m4)
label var d10_m1 "% applicable patients receiving a joint health and social care plan on discharge"
label var d10_m2 "% patients treated by a stroke-skilled Early Supported Discharge team"
label var d10_m3 "% applicable patients in atrial fibrillation discharged on anticoagulants"
label var d10_m4 "% patients discharged alive who are given a named person to contact"

rename _varname hospital
order hospital

* sort out time variables
foreach var of varlist *_c {
	tempvar t1 t2 v2
	gen `t1' = floor(`var') // hours
	gen `t2' = mod(`var',1) // _minutes_
	replace `t2' = `t2'/0.6  // so now "proportion of an hour"
		
	gen `v2' = `t1'+`t2'
		
	assert `v2' >= 	`var' 	if !missing(`var')
	assert `v2' <	`var'+1 if !missing(`var')
	assert !missing(`v2') 	if !missing(`var')
	
	replace `var' = `t1'+`t2'
	cap drop _*
}

rename (*_c) (*)

/* Remove non-hospitals */
drop if hospital == "National"

compress
save data/SSNAP_loaded, replace
