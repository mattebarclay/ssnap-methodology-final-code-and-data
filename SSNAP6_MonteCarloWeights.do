/******************************************************************************/
/* 	Produces the results plots etc based on the datasets produced in Combine
*/

cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"
set seed 53126

/******************************************************************************/
* Load data
use data/SSNAP_domains_values.dta, clear

drop if hospital == "National"

keep hospital d*_std1
desc

egen equal_summary_score = rowmean(d*_std1)
egen equal_summary_rank  = rank(equal_summary_score)
drop if missing(equal_summary_score)

expand 10000
sort hospital
by hospital: gen sim = _n

order sim
sort sim hospital

forval i = 1/10 {
	by sim: gen d`i'_wt = runiform(0,1) if _n == 1
	by sim: replace d`i'_wt = d`i'_wt[_n-1] if missing(d`i'_wt)
	
	gen d`i'_std1_wtd = d`i'_std1*d`i'_wt
	
}
egen totwt = rowtotal(d*_wt)

egen wtd_summary_score = rowtotal(*_wtd)
replace wtd_summary_score = wtd_summary_score/totwt

list sim hospital *summary_score in 1/4

egen sd_between = sd(wtd_summary_score), by(sim)
gen var_between = sd_between^2

egen wtd_summary_rank  = rank(wtd_summary_score), by(sim)

keep sim hospital equal_summary_score wtd_summary_score equal_summary_rank wtd_summary_rank var_between
save data/ssnap_MonteCarloSimulationLevel.dta, replace

/* Big summary plot */
use data/ssnap_MonteCarloSimulationLevel.dta, clear

#delimit ;
collapse 	(mean) 	mean		= wtd_summary_score 
			(min)	min  		= wtd_summary_score 
			(p25)  	lb 			= wtd_summary_score 
			(p75) 	ub 			= wtd_summary_score
			(max) 	max  		= wtd_summary_score
			(sd)  	sd_within 	= wtd_summary_score
			(mean) 	var_between = var_between
			(mean) 	mean_rank	= wtd_summary_rank 
			(min)	min_rank	= wtd_summary_rank 
			(p25)  	lb_rank		= wtd_summary_rank 
			(p75) 	ub_rank		= wtd_summary_rank
			(max) 	max_rank	= wtd_summary_rank
			, 	by(hospital equal_summary_score equal_summary_rank)
			;

#delimit cr

list in 1/4

sort equal_summary_score
list hospital mean equal_summary_score

gen e2 = equal_summary_score
sort e2 hospital
duplicates tag e2, gen(tag)
by e2: replace equal_summary_score = equal_summary_score+(_n-1)/5
by e2: replace equal_summary_rank  = equal_summary_rank +(_n-1)/2
scatter mean equal_summary_score

#delimit ;
twoway (
		rspike ub lb equal_summary_score  
		,	lcolor(blue%50)
	) 	
	(
		scatter mean equal_summary_score  
		,	msymb(+)
			mcolor(blue)
	)
	,	ytitle("Achievable scores")
		xtitle("Equal weights")
		title("Summary scores", span pos(11))
		ysc(noextend r(35 100) ) 
		xsc(noextend r(35 100) )
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			37 "E"
			, angle(h) tl(0) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			37 "E"
			, angle(h) tl(0) 
		)
		legend(
			order(2 "Mean summary score" 1 "50% of summary scores")
			ring(0) pos(4) cols(1)
		)
		ytick(40 60 70 80 100)
		xtick(40 60 70 80 100)
		ymtick(35, tl(0))
		xmtick(35, tl(0))
		yline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		name(MonteCarloWeights_summary, replace)
	;
	
	
	
#delimit cr
	
#delimit ;
twoway 		(
		rspike ub_rank lb_rank equal_summary_rank 
		,	lcolor(blue%50)
	) 	
	(
		scatter mean_rank equal_summary_rank
		,	msymb(+)
			mcolor(blue)
	)
	,	ytitle("Achievable ranks")
		xtitle("Equal weights")
		title("Summary ranks", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			1 "Worst"
			139 "Best"
			, angle(h) tl(0) 
		)
		xlabel(
			1 "Worst"
			139 "Best"
			, angle(h) tl(0) 
		)
		legend(
			off
		)
		ytick( 69 )
		xtick( 69 )
		yline(1 69 139, lcolor(gs8) lpattern(shortdash) noextend) 
		xline(1 69 139, lcolor(gs8) lpattern(shortdash) noextend)
		name(MonteCarloWeights_summaryranks, replace)
	;
	
graph combine MonteCarloWeights_summary MonteCarloWeights_summaryranks
	, cols(2)  name(montecarlo, replace);
graph export "results/figure12_ssnap_MonteCarloWeights.png", width(1000) replace ;
#delimit cr 

gen reliability = var_between / (sd_within^2 + var_between)

save data/ssnap_MonteCarloSummaries.dta, replace

use data/ssnap_MonteCarloSummaries.dta, clear



* a few plots
tempfile t

qui forval i = 1/10000 {
	if mod(`i',100) == 0 {
		nois di ". " _cont
		* reassure not stuck
	}
	
	use if sim == `i' using data/ssnap_MonteCarloSimulationLevel.dta, clear

	gen rho = .
	gen tau = .

	
	pwcorr wtd_summary_score equal_summary_score if sim == `i'
	by sim: replace rho = r(rho) if _n == 1 & sim == `i'
	
	ktau wtd_summary_score equal_summary_score if sim == `i'
	by sim: replace tau = r(tau_a) if _n == 1 & sim == `i'
	
	if `i' == 1 {
		save `t'
	}
	else {
		append using `t'
		save `t', replace
	}
}

use `t', clear

summ rho, meanonly
summ sim if rho == r(min)

local sim1 = r(max)
di "`sim1'"

local sim2 = ceil(runiform(0,10000))
local sim3 = ceil(runiform(0,10000))
local sim4 = ceil(runiform(0,10000))
local sim5 = ceil(runiform(0,10000))
local sim6 = ceil(runiform(0,10000))
local sim7 = ceil(runiform(0,10000))
local sim8 = ceil(runiform(0,10000))
local sim9 = ceil(runiform(0,10000))

summ tau, det

keep if inlist(sim,`sim1',`sim2',`sim3',`sim4',`sim5',`sim6',`sim7',`sim8',`sim9')

gen sim2 = 1 if sim == `sim1'
replace sim2 = 2 if sim == `sim2'
replace sim2 = 3 if sim == `sim3'
replace sim2 = 4 if sim == `sim4'
replace sim2 = 5 if sim == `sim5'
replace sim2 = 6 if sim == `sim6'
replace sim2 = 7 if sim == `sim7'
replace sim2 = 8 if sim == `sim8'
replace sim2 = 9 if sim == `sim9'

cap drop tau
gen tau = .
qui forval i = 1/9 {
	ktau wtd_summary_score equal_summary_score if sim2 == `i'
	local tau_`i' = string(r(tau_a),"%03.2f")
}

forval i = 1/9 {
	summ sim if sim2 == `i'
	local sim_n_`i' = r(mean)
}

#delimit ;
label define sim_plot	1 "Worst correlation (#`sim_n_1', Tau = `tau_1')"
						2 "#`sim_n_2', Kendall's Tau = `tau_2'"
						3 "#`sim_n_3', Kendall's Tau = `tau_3'"
						4 "#`sim_n_4', Kendall's Tau = `tau_4'"
						5 "#`sim_n_5', Kendall's Tau = `tau_5'"
						6 "#`sim_n_6', Kendall's Tau = `tau_6'"
						7 "#`sim_n_7', Kendall's Tau = `tau_7'"
						8 "#`sim_n_8', Kendall's Tau = `tau_8'"
						9 "#`sim_n_9', Kendall's Tau = `tau_9'"
						, replace;
#delimit cr
label values sim2 sim_plot


#delimit ;
twoway (
		scatter wtd_summary_score equal_summary_score  
		,	msymb(oh)
			mcolor(blue%50)
	)
	,	by(sim2, note("")) 
		ytitle("Alternative weighting schemes")
		xtitle("Equal weights")
		title("", span pos(11))
		subtitle(, lstyle(none) fcolor(none) size(small) )
		ysc(noextend r(35 100) ) 
		xsc(noextend r(35 100) )
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			37 "E"
			, angle(h) tl(0) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			37 "E"
			, angle(h) tl(0) 
		)
		legend(
			order(2 "Mean summary score" 1 "95% of summary scores")
			ring(0) pos(4) cols(1)
		)
		ytick(40 60 70 80 100)
		;
		
* Change of >10
use data/ssnap_MonteCarloSimulationLevel.dta, clear


gen random_grade = ///
	cond(wtd_summary_score >= 80, "A", ///
		cond(wtd_summary_score >= 70, "B", ///
			cond(wtd_summary_score >= 60, "C", ///
				cond(wtd_summary_score >= 40, "D", "E"))))

gen equal_grade = ///
	cond(equal_summary_score >= 80, "A", ///
		cond(equal_summary_score >= 70, "B", ///
			cond(equal_summary_score >= 60, "C", ///
				cond(equal_summary_score >= 40, "D", "E"))))

count
count if random_grade != equal_grade

gen change = random_grade != equal_grade

gen one = 1
table one, c(freq sum change mean change)

table equal_grade, c(freq sum change mean change)
/*		xtick(40 60 70 80 100)
		ymtick(35, tl(0))
		xmtick(35, tl(0))
		yline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		name(MonteCarloWeights_individuals, replace)
	;*/
graph export "results/figure13_ssnap_mc_individual.png", width(1000) replace ;

* An example
use data/ssnap_MonteCarloSimulationLevel.dta, clear

gen equal_grade = ///
	cond(equal_summary_score >= 80, "A", ///
		cond(equal_summary_score >= 70, "B", ///
			cond(equal_summary_score >= 60, "C", ///
				cond(equal_summary_score >= 40, "D", "E"))))


gen random_grade = ///
	cond(wtd_summary_score >= 80, "A", ///
		cond(wtd_summary_score >= 70, "B", ///
			cond(wtd_summary_score >= 60, "C", ///
				cond(equal_summary_score >= 40, "D", "E"))))

gen agree = equal_grade == random_grade

collapse (sum) agree, by(hospital equal_grade)
tab agree if equal_grade == "C"
summ agree if equal_grade == "C"
// mean = 6487
sort agree
list hospital agree if equal_grade == "C"

list if agree == 6514 //  ChesterfieldRoyal - close to mean

use data/ssnap_MonteCarloSimulationLevel.dta, clear

keep if hospital == "ChesterfieldRoyal"

count if wtd_summary_score > 80

gen equal_grade = ///
	cond(equal_summary_score >= 80, "A", ///
		cond(equal_summary_score >= 70, "B", ///
			cond(equal_summary_score >= 60, "C", ///
				cond(equal_summary_score >= 40, "D", "E"))))


gen random_grade = ///
	cond(wtd_summary_score >= 80, "A", ///
		cond(wtd_summary_score >= 70, "B", ///
			cond(wtd_summary_score >= 60, "C", ///
				cond(equal_summary_score >= 40, "D", "E"))))
			
tab random_grade


