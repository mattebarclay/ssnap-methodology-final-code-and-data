/******************************************************************************/
/* 	Produces the results plots etc based on the datasets produced in Combine
*/

cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"

/******************************************************************************/
* Load data
use data/SSNAP_final.dta, clear

gen grade_current = ///
	cond(d_std1_equal >= 80, "A", ///
		cond(d_std1_equal >= 70, "B", /// 
			cond(d_std1_equal >= 60, "C", ///
				cond(d_std1_equal >= 40, "D", /// 
					cond(d_std1_equal >= 0, "E", "")))))
assert !missing(grade_current)


/******************************************************************************/
* Matrix plot
/*
rename (*_nr_*) (*_nr_*_ignore)

#delimit ;
graph matrix *_rank
	, 	msymb(oh) mcolor(purple%50) 
		half
		title("", size(medium))
		maxes(
			xlabel(0 70 140)
			ylabel(0 70 140)
			xtick(35 105)
			ytick(35 105)
			ysc(r(-10 150))
			xsc(r(-10 150))
		)
		diagopts(size(small))
		ysize(4)
		xsize(4)
		name(matrix, replace)
;
#delimit cr
graph export "ssnap_correlations_2020-06-26.png", width(1000) replace

rename (*_nr_*_ignore) (*_nr_*) 
*/

/******************************************************************************/
* As-is vs no preliminary banding
ktau d_std1_interp_equal d_std1_equal
local r = string(r(tau_a),"%03.2f")

#delimit ;
twoway 	(
		scatter d_std1_interp_equal d_std1_equal
		,	msymb(oh) mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 99) lcolor(gs8) )
	,	ytitle("Using interpolation istead of banding")
		xtitle("Current approach")
		title("Summary scores")
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			90 "A" 
			75 "B"
			65 "C"  
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		legend(
			order(1 "Hospital" 2 "Line of equality")
			ring(0) pos(5) cols(1)
		)
		ytick(0 40 60 70 80 100)
		xtick(0 40 60 70 80 100)
		yline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		text(94 -1 "Kendall's {&tau} = `r'", placement(3) )
		name(actual_v_no_pre_banding, replace)
	;
#delimit cr
	
local y_var d_std1_interp_equal	
*summ `y_var'_rank if `y_var' <  40
*local e_y_line = r(max)+0.5
local e_y_line = 0
summ `y_var'_rank if `y_var' <  60
local d_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  70
local c_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  80
local b_y_line = r(max)+0.5

summ d_std1_equal_rank if d_std1_equal <  40
local e_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  60
local d_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  70
local c_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  80
local b_x_line = r(max)+0.5
	
#delimit ;
twoway 		(
		scatter d_std1_interp_equal_rank d_std1_equal_rank
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 139) lcolor(gs8) )
	,	ytitle("Using interpolation istead of banding")
		xtitle("Current approach")
		title("Summary ranks", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			/*`=`e_y_line'/2' "E" */
			/*`=`e_y_line'+`=`d_y_line'-`e_y_line''/2' "D"*/
			`=`d_y_line'+`=`c_y_line'-`d_y_line''/2' "C"  
			`=`c_y_line'+`=`b_y_line'-`c_y_line''/2' "B"
			`=`b_y_line'+`=`=_N'-`b_y_line''/2' "A"
			, angle(h) tl(0) 
		)
		xlabel(
			/*`=`e_x_line'/2' "E" */
			/*`=`e_x_line'+`=`d_x_line'-`e_x_line''/2' "D"*/
			`=`d_x_line'+`=`c_x_line'-`d_x_line''/2' "C"  
			`=`c_x_line'+`=`b_x_line'-`c_x_line''/2' "B"
			`=`b_x_line'+`=`=_N'-`b_x_line''/2' "A"
			, angle(h) tl(0) 
		)
		legend(
			off
		)
		ytick(1 /*`e_y_line'*/ `d_y_line' `c_y_line' `b_y_line' `=_N')
		xtick(1 `e_x_line' `d_x_line' `c_x_line' `b_x_line' `=_N')
		yline( `e_y_line' `d_y_line' `c_y_line' `b_y_line'   , lcolor(gs8) lpattern(shortdash) noextend) 
		xline( `e_x_line' `d_x_line' `c_x_line' `b_x_line'   , lcolor(gs8) lpattern(shortdash) noextend)
		name(actual_v_no_pre_banding_ranks, replace)
	;

graph combine actual_v_no_pre_banding actual_v_no_pre_banding_ranks
	, cols(2)  name(preband, replace);

graph export "results/figure10_ssnap_actual_v_no_rding.png", width(1000) replace ;
#delimit cr 

local measure d_std1_interp_equal 
gen grade_`measure' = ///
	cond(`measure' >= 80, "A", ///
		cond(`measure' >= 70, "B", /// 
			cond(`measure' >= 60, "C", ///
				cond(`measure' >= 40, "D", /// 
					cond(`measure' >= 0, "E", "")))))
assert !missing(grade_current)

tab grade_`measure' grade_current

* difference between scores
gen diff = d_std1_interp_equal-d_std1_equal
count if diff == 0
count if diff > 0
count if diff < 0

gen sd = sign(diff)
bys sd: summ(d_std1_equal)

gen a = grade_current == "A"
bys a: summ(diff)

* Only keep the preliminary standardised ones
drop *_interp*

/******************************************************************************/
* Actual vs Reference standardisation
ktau d_std2_equal d_std1_equal 
local ra = string(r(tau_a),"%03.2f")

#delimit ;
twoway 	(
		scatter d_std2_equal d_std1_equal  
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	,	ytitle("Z-scoring")
		xtitle("Current approach")
		title("Summary scores", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			-2(1)2
			, angle(h) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		ytick(-2.2 2.2, tl(0))
		xtick(0 40 60 70 80 100)
		yline(-2 -1 0 1 2, lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		text(1.7 -1 "Kendall's {&tau} = `ra'", placement(3) )
		name(actual_v_zscore, replace)
	;
	
#delimit cr
	
local y_var d_std2_equal	
summ `y_var'_rank if `y_var' <  -1
local m1_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  0
local m0_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  1
local p1_y_line = r(max)+0.5

summ d_std1_equal_rank if d_std1_equal <  40
local e_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  60
local d_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  70
local c_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  80
local b_x_line = r(max)+0.5
	
#delimit ;
twoway 		(
		scatter d_std2_equal_rank d_std1_equal_rank
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 139) lcolor(gs8) )
	,	ytitle("Z-scoring")
		xtitle("Current approach")
		title("Summary ranks", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			`m1_y_line' "-1"
			`m0_y_line' "0"
			`p1_y_line' "1"
			, angle(h) tl(0) 
		)
		xlabel(
			/*`=`e_x_line'/2' "E" */
			/*`=`e_x_line'+`=`d_x_line'-`e_x_line''/2' "D"*/
			`=`d_x_line'+`=`c_x_line'-`d_x_line''/2' "C"  
			`=`c_x_line'+`=`b_x_line'-`c_x_line''/2' "B"
			`=`b_x_line'+`=`=_N'-`b_x_line''/2' "A"
			, angle(h) tl(0) 
		)
		legend(
			off
		)
		ytick(`m1_y_line' `m0_y_line' `p1_y_line')
		xtick(1 `e_x_line' `d_x_line' `c_x_line' `b_x_line' `=_N')
		yline( `m1_y_line' `m0_y_line' `p1_y_line'    , lcolor(gs8) lpattern(shortdash) noextend) 
		xline( `e_x_line' `d_x_line' `c_x_line' `b_x_line'   , lcolor(gs8) lpattern(shortdash) noextend)
		name(actual_v_zscore_ranks, replace)
	;
	
graph combine actual_v_zscore actual_v_zscore_ranks
		, cols(2)  name(zscore, replace);

graph export "results/figure16_ssnap_actual_v_z_2020-07-02.png", width(1000) replace ;
#delimit cr 

/******************************************************************************/
* Actual vs EFA domains
ktau efa_std1_equal d_std1_equal 
local rb = string(r(tau_a),"%03.2f")

#delimit ;
twoway 		(
		scatter efa_std1_equal d_std1_equal  
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 99) lcolor(gs8) )
	,	ytitle("Domains from EFA")
		xtitle("Current approach")
		title("Summary scores", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			90 "A" 
			75 "B"
			65 "C"  
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		legend(
			order(1 "Hospital" 2 "Line of equality")
			ring(0) pos(5) cols(1)
		)
		ytick(0 40 60 70 80 100)
		xtick(0 40 60 70 80 100)
		yline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		text(94 -1 "Kendall's {&tau} = `rb'", placement(3) )
		name(actual_v_efa, replace)
	;
	
#delimit cr
	
local y_var efa_std1_equal	
/*summ `y_var'_rank if `y_var' <  40
local e_y_line = r(max)+0.5*/
local e_y_line = 0
summ `y_var'_rank if `y_var' <  60
local d_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  70
local c_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  80
local b_y_line = r(max)+0.5

summ d_std1_equal_rank if d_std1_equal <  40
local e_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  60
local d_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  70
local c_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  80
local b_x_line = r(max)+0.5
	
#delimit ;
twoway 		(
		scatter efa_std1_equal_rank d_std1_equal_rank
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 139) lcolor(gs8) )
	,	ytitle("Domains from EFA")
		xtitle("Current approach")
		title("Summary ranks", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			/*`=`e_y_line'/2' "E" */
			/*`=`e_y_line'+`=`d_y_line'-`e_y_line''/2' "D"*/
			`=`d_y_line'+`=`c_y_line'-`d_y_line''/2' "C"  
			`=`c_y_line'+`=`b_y_line'-`c_y_line''/2' "B"
			`=`b_y_line'+`=`=_N'-`b_y_line''/2' "A"
			, angle(h) tl(0) 
		)
		xlabel(
			/*`=`e_x_line'/2' "E" */
			/*`=`e_x_line'+`=`d_x_line'-`e_x_line''/2' "D"*/
			`=`d_x_line'+`=`c_x_line'-`d_x_line''/2' "C"  
			`=`c_x_line'+`=`b_x_line'-`c_x_line''/2' "B"
			`=`b_x_line'+`=`=_N'-`b_x_line''/2' "A"
			, angle(h) tl(0) 
		)
		legend(
			off
		)
		ytick(1 /*`e_y_line'*/ `d_y_line' `c_y_line' `b_y_line' `=_N')
		xtick(1 `e_x_line' `d_x_line' `c_x_line' `b_x_line' `=_N')
		yline( `e_y_line' `d_y_line' `c_y_line' `b_y_line'   , lcolor(gs8) lpattern(shortdash) noextend) 
		xline( `e_x_line' `d_x_line' `c_x_line' `b_x_line'   , lcolor(gs8) lpattern(shortdash) noextend)
		name(actual_v_efa_ranks, replace)
	;
	
graph combine actual_v_efa actual_v_efa_ranks
	, cols(2)  name(efa, replace);
graph export "results/figure15_ssnap_actual_v_efa_2020-07-02.png", width(1000) replace ;
#delimit cr




local measure efa_std1_equal 
gen grade_`measure' = ///
	cond(`measure' >= 80, "A", ///
		cond(`measure' >= 70, "B", /// 
			cond(`measure' >= 60, "C", ///
				cond(`measure' >= 40, "D", /// 
					cond(`measure' >= 0, "E", "")))))
assert !missing(grade_current)

tab grade_`measure' grade_current


/******************************************************************************/
* Actual vs policy weights
ktau d_std1_policy d_std1_equal 
local rc = string(r(tau_a),"%03.2f")

#delimit ;
twoway 		(
		scatter d_std1_policy d_std1_equal  
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 99) lcolor(gs8) )
	,	ytitle("Overweighting 'outcome' domains")
		xtitle("Current approach")
		title("Summary scores", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			90 "A" 
			75 "B"
			65 "C"  
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		legend(
			order(1 "Hospital" 2 "Line of equality")
			ring(0) pos(5) cols(1)
		)
		ytick(0 40 60 70 80 100)
		xtick(0 40 60 70 80 100)
		yline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 , lcolor(gs8) lpattern(shortdash) noextend)
		text(94 -1 "Kendall's {&tau} = `rc'", placement(3) )
		name(actual_v_policy, replace)
	;
	
#delimit cr
	
local y_var d_std1_policy	
summ `y_var'_rank if `y_var' <  40
local e_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  60
local d_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  70
local c_y_line = r(max)+0.5
summ `y_var'_rank if `y_var' <  80
local b_y_line = r(max)+0.5

summ d_std1_equal_rank if d_std1_equal <  40
local e_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  60
local d_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  70
local c_x_line = r(max)+0.5
summ d_std1_equal_rank if d_std1_equal <  80
local b_x_line = r(max)+0.5
	
#delimit ;
twoway 		(
		scatter d_std1_policy_rank d_std1_equal_rank
		,	msymb(oh)
			mcolor(blue%50)
			jitter(1)
	)
	( function y=x, range(1 139) lcolor(gs8) )
	,	ytitle("Overweighting 'outcome' domains")
		xtitle("Current approach")
		title("Summary ranks", span pos(11))
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		ylabel(
			/*`=`e_y_line'/2' "E" */
			/*`=`e_y_line'+`=`d_y_line'-`e_y_line''/2' "D"*/
			`=`d_y_line'+`=`c_y_line'-`d_y_line''/2' "C"  
			`=`c_y_line'+`=`b_y_line'-`c_y_line''/2' "B"
			`=`b_y_line'+`=`=_N'-`b_y_line''/2' "A"
			, angle(h) tl(0) 
		)
		xlabel(
			/*`=`e_x_line'/2' "E" */
			/*`=`e_x_line'+`=`d_x_line'-`e_x_line''/2' "D"*/
			`=`d_x_line'+`=`c_x_line'-`d_x_line''/2' "C"  
			`=`c_x_line'+`=`b_x_line'-`c_x_line''/2' "B"
			`=`b_x_line'+`=`=_N'-`b_x_line''/2' "A"
			, angle(h) tl(0) 
		)
		legend(
			off
		)
		ytick(1 `e_y_line' `d_y_line' `c_y_line' `b_y_line' `=_N')
		xtick(1 `e_x_line' `d_x_line' `c_x_line' `b_x_line' `=_N')
		yline( `e_y_line' `d_y_line' `c_y_line' `b_y_line'   , lcolor(gs8) lpattern(shortdash) noextend) 
		xline( `e_x_line' `d_x_line' `c_x_line' `b_x_line'   , lcolor(gs8) lpattern(shortdash) noextend)
		name(actual_v_policy_ranks, replace)
	;
	
graph combine actual_v_policy actual_v_policy_ranks
	, cols(2)  name(weights, replace);
graph export "results/figure11_ssnap_actual_v_policy_2020-07-02.png", width(1000) replace ;

#delimit cr


local measure d_std1_policy 
gen grade_`measure' = ///
	cond(`measure' >= 80, "A", ///
		cond(`measure' >= 70, "B", /// 
			cond(`measure' >= 60, "C", ///
				cond(`measure' >= 40, "D", /// 
					cond(`measure' >= 0, "E", "")))))
assert !missing(grade_`measure')

tab grade_`measure' grade_current

/******************************************************************************/
* Combine
*graph combine actual_v_zscore actual_v_efa actual_v_policy, cols(1) ysize(10)
*graph export "ssnap_single_changes_2020-06-26.png", width(1000) replace 

* summary numbers
desc

rename (d_*) (grp1_*)
rename (efa_*) (grp2_*)
rename (*_equal) (*_wt1)
rename (*_policy) (*_wt2)

foreach grp in 1 2 {
	foreach wt in 1 2 {
		#delimit ;
		gen grp`grp'_std1_wt`wt'_cat =
			cond(grp`grp'_std1_wt`wt' > 80, 1, /* A */
				cond(grp`grp'_std1_wt`wt' > 70, 2, /* B */
						cond(grp`grp'_std1_wt`wt' > 60, 3, /* C */
							cond(grp`grp'_std1_wt`wt' > 40, 4, /* D */
								5 /* E */
							)
						)
					)
				)
			;
		#delimit cr
	}
}

/* Program to speed up Ktau... */
* Only changing approach to standardising measures
cap program drop ktau_ing
program define ktau_ing
	syntax , std(int) grp(int) wt(int) output(string)
		
	ktau grp`grp'_std`std'_wt`wt' grp1_std1_wt1 
	local tau_all = r(tau_a)
	
	if `std' == 1 {
		count if abs(grp`grp'_std`std'_wt`wt'_cat - grp1_std1_wt1_cat) == 0
		local n0 = r(N)
		
		count if abs(grp`grp'_std`std'_wt`wt'_cat - grp1_std1_wt1_cat) == 1
		local n1 = r(N)
		
		count if abs(grp`grp'_std`std'_wt`wt'_cat - grp1_std1_wt1_cat) >= 2
		local n2 = r(N)
	}
	else if `std' != 1 {
		local n0 = .
		local n1 = .
		local n2 = .
	}
		
	matrix `output' = (`tau_all', ., `n0', ., `n1', ., `n2', .)
end

* Only changing approach to standardising measures
ktau_ing, std(2) grp(1) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results_part

* Only changing approach to grouping measures
ktau_ing, std(1) grp(2) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Only changing weights given to measure domains
ktau_ing, std(1) grp(1) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation and grouping
ktau_ing, std(2) grp(2) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation and weights
ktau_ing, std(2) grp(1) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing weights and grouping
ktau_ing, std(1) grp(2) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation, grouping and weights
ktau_ing, std(2) grp(2) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

putexcel set results/ssnap_raw_results.xlsx, sheet(ktau, replace) replace
#delimit ;
putexcel 	C1=("Kendall's Tau correlation coefficient") 
			E1=("Number with grade unaffected")
			G1=("Number moving one grade") 
			I1=("Number moving two or more grades") 
			B2=("Only changing approach to standardising measures")
			B3=("Only changing approach to grouping measures")
			B4=("Only changing weights given to measure domains")
			B5=("Changing standardisation and grouping")
			B6=("Changing standardisation and weights")
			B7=("Changing grouping and weights")
			B8=("Changing standardisation, grouping and weights")
			C2=matrix(ktau_results)
			;
#delimit cr

