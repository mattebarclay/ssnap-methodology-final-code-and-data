/******************************************************************************/
/* 	Produces the results plots etc based on the datasets produced in Combine
*/

cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"

/******************************************************************************/
* Load data
use data/SSNAP_domains.dta, clear
keep hospital d*

desc

rename (d#_std1 d#_std2) (std1_d# std2_d#)

reshape long std1_d std2_d, i(hospital) j(domain)
label define domain	1	"Scanning" /// 
					2	"Stroke unit" /// 
					3	"Thrombolysis" /// 
					4	"Specialist assessments" /// 
					5	"Occupational therapy" /// 
					6	"Physiotherapy" /// 
					7	"Speech and language therapy" /// 
					8	"MDT working" /// 
					9	"Standards by discharge" /// 
					10	"Discharge processes" ///
					,	replace
label values domain domain

#delimit ;
twoway	(
		scatter std2_d std1_d
		,	msymb(oh)
			mcolor(blue%50)
	) 
	,	by(
			domain
			,	cols(2)
				iyaxes ixaxes
				note("Note: For the different therapy domains, scores of over 100 are possible.", span)
		)
		ytitle("Standardised using Z-scores")
		xtitle("Current approach")
		ylabel(
			-6(2)2
			, angle(h) 
		)
		xlabel(
			90 "A" 
			75 "B"
			65 "C"
			50 "D"
			20 "E"
			, angle(h) tl(0) 
		)
		ysc(noextend) 
		plotregion(lstyle(none))
		xsc(noextend)
		xtick(0 40 60 70 80 100)
		yline(-6(2)2		, lcolor(gs8) lpattern(shortdash) noextend) 
		xline(40 60 70 80 	, lcolor(gs8) lpattern(shortdash) noextend)
		xsize(4)
		ysize(8)
	;
#delimit cr
graph export "results/figure16_ssnap_individual_domains.png", width(1000) replace
