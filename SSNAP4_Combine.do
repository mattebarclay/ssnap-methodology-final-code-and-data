/******************************************************************************/
/* 	Combine data and calculate overall ranks
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\ssnap-methodology-final-code-and-data"


/******************************************************************************/
/* Load data																  */
use data/SSNAP_grouped.dta, clear

/******************************************************************************/
/* Generate domain scores 													  */
keep hospital *_std1 *_std2

forval domain = 1/10 {
	egen d`domain'_std1 = rowmean(d`domain'_*_std1)
	egen d`domain'_std2 = rowmean(d`domain'_*_std2)
}

label var d1_std1 "Scanning - SSNAP"
label var d1_std2 "Scanning - Z-score"
label var d2_std1 "Stroke unit - SSNAP"
label var d2_std2 "Stroke unit - Z-score"
label var d3_std1 "Thrombolysis - SSNAP"
label var d3_std2 "Thrombolysis - Z-score"
label var d4_std1 "Specialist assessments - SSNAP"
label var d4_std2 "Specialist assessments - Z-score"
label var d5_std1 "Occupational therapy - SSNAP"
label var d5_std2 "Occupational therapy - Z-score"
label var d6_std1 "Physiotherapy - SSNAP"
label var d6_std2 "Physiotherapy - Z-score"
label var d7_std1 "Speech and language therapy - SSNAP"
label var d7_std2 "Speech and language therapy - Z-score"
label var d8_std1 "MDT working - SSNAP"
label var d8_std2 "MDT working - Z-score"
label var d9_std1 "Standards by discharge - SSNAP"
label var d9_std2 "Standards by discharge - Z-score"
label var d10_std1 "Discharge processes - SSNAP"
label var d10_std2 "Discharge processes - Z-score"


forval domain = 1/7 {
	egen efa`domain'_std1 = rowmean(efa1_`domain'_*)
	egen efa`domain'_std2 = rowmean(efa2_`domain'_*)
}

label var efa1_std1 "Good care - SSNAP" 
label var efa1_std2 "Good care - Z-score"
label var efa2_std1 "Stroke unit - SSNAP"
label var efa2_std2 "Stroke unit - Z-score"
label var efa3_std1 "Occupational therapy and physio - SSNAP"
label var efa3_std2 "Occupational therapy and physio - Z-score"
label var efa4_std1 "Identification of therapy needs - SSNAP"
label var efa4_std2 "Identification of therapy needs - Z-score"
label var efa5_std1 "Rapid thrombolysis - SSNAP"
label var efa5_std2 "Rapid thrombolysis - Z-score"
label var efa6_std1 "Speech and language therapy - SSNAP"
label var efa6_std2 "Speech and language therapy - Z-score"
label var efa7_std1 "Daily time receiving therapy - SSNAP"
label var efa7_std2 "Daily time receiving therapy - Z-score"

keep hospital d?_std1 d??_std1 d?_std2 d??_std2 efa?_std1 efa?_std2
desc
save data/SSNAP_domains.dta, replace


/******************************************************************************/
/* Combine domain scores 													  */
use data/SSNAP_domains.dta, clear


/* From protocol:											  

	Existing approach: 
		Equal weights.
		
	Alternate approach: 
		Give 70% of the weight to the four acute domains 
		(Scanning, Stroke unit, Thrombolysis, Specialist assessment), 
		split evenly, and 30% of the weight to the six domains covering 
		recovery and discharge.
		
		- For EFA domains, give 70% of weight to
		Good care, Stroke unit, Rapid thrombolysis

*/

/* DOMAIN GRADES */

/*
Once the level for each of the domains has been determined each level is 
assigned a number of points (A=100, B=80, C=60, D=40, E=20). For example, an 
average score of 78% for patient-centred Domain 10 would be a Level C 
performance. Level C would be worth 60 points towards the patient-centred 
Total Key Indicator score.
*/

forval i = 1/10 {
	gen d`i'_std1_raw = d`i'_std1
	local lab : variable label d`i'_std1
	local lab "`lab' raw score"
	label variable d`i'_std1_raw "`lab'"
	
	gen d`i'_std1_interp = .
	local lab : variable label d`i'_std1
	local lab "`lab' interpolated"
	label variable d`i'_std1_interp "`lab'"
	
	replace d`i'_std1 = .
	
	* Each SSNAP domain has different rounding approach !?
	* Score criteria...
	local a_score 100
	local b_score  80
	local c_score  60
	local d_score  40
	local e_score  20
	
	* Required performance for each score for each domain...
	if `i' == 1 {
		local a_perf  95
		local b_perf  85
		local c_perf  70
		local d_perf  55
	}
	if `i' == 2 {
		local a_perf  90
		local b_perf  80
		local c_perf  70
		local d_perf  60
	}
	if `i' == 3 {
		local a_perf  80
		local b_perf  70
		local c_perf  60
		local d_perf  45
	}
	if `i' == 4 {
		local a_perf  90
		local b_perf  80
		local c_perf  75
		local d_perf  65
	}
	if `i' == 5 {
		local a_perf  80
		local b_perf  75
		local c_perf  65
		local d_perf  60
	}
	if `i' == 6 {
		local a_perf  85
		local b_perf  75
		local c_perf  70
		local d_perf  60
	}
	if `i' == 7 {
		local a_perf  75
		local b_perf  65
		local c_perf  55
		local d_perf  50
	}
	if `i' == 8 {
		local a_perf  85
		local b_perf  80
		local c_perf  75
		local d_perf  65
	}
	if `i' == 9 {
		local a_perf  95
		local b_perf  80
		local c_perf  70
		local d_perf  55
	}
	if `i' == 10 {
		local a_perf  95
		local b_perf  85
		local c_perf  75
		local d_perf  60
	}
	
	* Interpolated domain scores	
	replace d`i'_std1_interp = `a_score' ///
			if d`i'_std1_raw >= `a_perf' & missing(d`i'_std1_interp)
		
	replace d`i'_std1_interp = ///
			`b_score' /// 
			+ (`a_score'-`b_score')*(d`i'_std1_raw-`b_perf')/(`a_perf'-`b_perf') ///
		if d`i'_std1_raw >= `b_perf' & missing(d`i'_std1_interp)
		
		list d`i'_std1_interp d`i'_std1_raw in 1/50
		
	replace d`i'_std1_interp = ///
			`c_score' /// 
			+ (`b_score'-`c_score')*(d`i'_std1_raw-`c_perf')/(`b_perf'-`c_perf') ///
		if d`i'_std1_raw >= `c_perf' & missing(d`i'_std1_interp)
		
	replace d`i'_std1_interp = ///
			`d_score' /// 
			+ (`c_score'-`d_score')*(d`i'_std1_raw-`d_perf')/(`c_perf'-`d_perf') ///
		if d`i'_std1_raw >= `d_perf' & missing(d`i'_std1_interp)
		
	replace d`i'_std1_interp = ///
			`e_score' /// 
			+ (`d_score'-`e_score')*(d`i'_std1_raw)/(`d_perf') ///
		if missing(d`i'_std1_interp)

	replace d`i'_std1 = `a_score' if d`i'_std1_raw >= `a_perf' & missing(d`i'_std1)
	replace d`i'_std1 = `b_score' if d`i'_std1_raw >= `b_perf' & missing(d`i'_std1)
	replace d`i'_std1 = `c_score' if d`i'_std1_raw >= `c_perf' & missing(d`i'_std1)
	replace d`i'_std1 = `d_score' if d`i'_std1_raw >= `d_perf' & missing(d`i'_std1)
	replace d`i'_std1 = `e_score' if d`i'_std1_raw >= 0		   & missing(d`i'_std1)
	
	assert !missing(d`i'_std1)
	
	/*
	#delimit ;
	replace d`i'_std1 = 
		cond(d`i'_std1_raw >= `a_perf', `a_score', /* A */
			cond(d`i'_std1_raw >= `b_perf', `b_score', /* B */
					cond(d`i'_std1_raw >= `c_perf', `c_score', /* C */
						cond(d`i'_std1_raw >= `d_perf', `d_score', /* D */
							`e_score' /* E */
						)
					)
				)
			)
		;
	#delimit cr
	*/
	
	* Rounding for the EFA domains
	if `i' == 1 {
		* Good care - match "MDT working"
	
		local a_perf  85
		local b_perf  80
		local c_perf  75
		local d_perf  65
	}
	if `i' == 2 {
		* Stroke unit - match "Stroke unit"
		local a_perf  90
		local b_perf  80
		local c_perf  70
		local d_perf  60
	}
	if `i' == 3 {
		* Rapid thrombolysis - match thrombolysis
		local a_perf  80
		local b_perf  70
		local c_perf  60
		local d_perf  45
	}
	if `i' == 4 {
		* Receipt of therapy - match Occ therapy
		local a_perf  80
		local b_perf  75
		local c_perf  65
		local d_perf  60
	}
	if `i' == 5 {
		* Identification of therapy need - match Occ therapy
		local a_perf  80
		local b_perf  75
		local c_perf  65
		local d_perf  60
	}
	if `i' == 6 {
		* Receipt of SLT - match SLT
		local a_perf  75
		local b_perf  65
		local c_perf  55
		local d_perf  50
	}
	if `i' == 7 {
		* Time receiving therapy - match Physiotherapy
		local a_perf  85
		local b_perf  75
		local c_perf  70
		local d_perf  60
	}
	
	* Rounded domain scores
	if `i' <=7 {
		gen efa`i'_std1_raw = efa`i'_std1
		local lab : variable label efa`i'_std1
		local lab "`lab' raw score"
		label variable efa`i'_std1_raw "`lab'"	

		replace efa`i'_std1 = .
		
		replace efa`i'_std1 = `a_score' if efa`i'_std1_raw >= `a_perf' & missing(efa`i'_std1)
		replace efa`i'_std1 = `b_score' if efa`i'_std1_raw >= `b_perf' & missing(efa`i'_std1)
		replace efa`i'_std1 = `c_score' if efa`i'_std1_raw >= `c_perf' & missing(efa`i'_std1)
		replace efa`i'_std1 = `d_score' if efa`i'_std1_raw >= `d_perf' & missing(efa`i'_std1)
		replace efa`i'_std1 = `e_score' if efa`i'_std1_raw >= 0		   & missing(efa`i'_std1)
		
		assert !missing(efa`i'_std1)
	}

}

/* Sanity check */
forval i = 1/10 {
	nois di "`i'"
	assert d`i'_std1_interp >= d`i'_std1
}

/* WEIGHTS ARE NOW CORRECT */
gen d1_wt  = 3.5 if !missing(d1_std1)
gen d2_wt  = 3.5 if !missing(d2_std1)
gen d3_wt  = 3.5 if !missing(d3_std1)
gen d4_wt  = 3.5 if !missing(d4_std1)
gen d5_wt  = 1.0 if !missing(d5_std1)
gen d6_wt  = 1.0 if !missing(d6_std1)
gen d7_wt  = 1.0 if !missing(d7_std1)
gen d8_wt  = 1.0 if !missing(d8_std1)
gen d9_wt  = 1.0 if !missing(d9_std1)
gen d10_wt = 1.0 if !missing(d10_std1)

gen efa1_wt = 3.1 if !missing(efa1_std1)
gen efa2_wt = 3.1 if !missing(efa2_std1) 
gen efa3_wt = 1.0 if !missing(efa3_std1)
gen efa4_wt = 1.0 if !missing(efa4_std1)
gen efa5_wt = 3.1 if !missing(efa5_std1)
gen efa6_wt = 1.0 if !missing(efa6_std1)
gen efa7_wt = 1.0 if !missing(efa7_std1)

save data/SSNAP_domains_values.dta, replace

/* Weighted domain scores */
forval i = 1/10 {
	gen d`i'_std1_wtd    		= d`i'_std1   *d`i'_wt
	gen d`i'_std1_wtd_interp 	= d`i'_std1_interp*d`i'_wt
	gen d`i'_std2_wtd    		= d`i'_std2   *d`i'_wt
}
forval i = 1/7 {
	gen efa`i'_std1_wtd    		= efa`i'_std1   *d`i'_wt
	gen efa`i'_std2_wtd    		= efa`i'_std2   *d`i'_wt
}

/* Composites using "policy-based" weights */
egen d_std1_policy      	= rowtotal(d*_std1_wtd)
egen d_std1_interp_policy   = rowtotal(d*_std1_wtd)
egen d_std2_policy      	= rowtotal(d*_std2_wtd)
egen efa_std1_policy    	= rowtotal(efa*_std1_wtd)
egen efa_std1_interp_policy = rowtotal(efa*_std1_wtd)
egen efa_std2_policy    	= rowtotal(efa*_std2_wtd)

egen d_tot_wts   = rowtotal(d*_wt)
egen efa_tot_wts = rowtotal(efa*_wt)

replace d_std1_policy     		= d_std1_policy/d_tot_wts
replace d_std1_interp_policy   	= d_std1_interp_policy/d_tot_wts
replace d_std2_policy      		= d_std2_policy/d_tot_wts
replace efa_std1_policy    		= efa_std1_policy/efa_tot_wts
replace efa_std1_interp_policy 	= efa_std1_interp_policy/efa_tot_wts
replace efa_std2_policy    		= efa_std2_policy/efa_tot_wts

/* Composites using equal weights */
egen d_std1_equal 	    	= rowmean(d*_std1)
egen d_std1_interp_equal 	= rowmean(d*_std1_interp)
egen d_std2_equal 	    	= rowmean(d*_std2)
egen efa_std1_equal     	= rowmean(efa*_std1)
egen efa_std2_equal     	= rowmean(efa*_std2)

label var d_std1_equal "As in SSNAP (assigned, equal wts, absolute)"
label var d_std2_equal "Assigned, equal wts, Z-scores"
label var efa_std1_equal "EFA, equal wts, absolute"
label var efa_std2_equal "EFA, equal wts, Z-scores"
label var d_std1_policy "Assigned, 'policy' wts, absolute"
label var d_std2_policy "Assigned, 'policy' wts, Z-scores"
label var efa_std1_policy "EFA, 'policy' wts, absolute"
label var efa_std2_policy "EFA, 'policy' wts, Z-scores"
label var d_std1_interp_equal "As in SSNAP, interpolated"
label var d_std1_interp_policy "Policy-wts and interpolated"
label var efa_std1_interp_policy "EFA domains, policy wts, and interpolated"

keep hospital 	d_std1_equal /// 
				d_std2_equal ///
				efa_std1_equal /// 
				efa_std2_equal /// 
				d_std1_policy /// 
				d_std2_policy /// 
				efa_std1_policy /// 
				efa_std2_policy ///
				d_std1_interp_equal /// 
				d_std1_interp_policy /// 
				efa_std1_interp_policy  
	
foreach thing in d_std1_equal /// 
				d_std2_equal ///
				efa_std1_equal /// 
				efa_std2_equal /// 
				d_std1_policy /// 
				d_std2_policy /// 
				efa_std1_policy /// 
				efa_std2_policy	/// 
				d_std1_interp_equal /// 
				d_std1_interp_policy /// 
				efa_std1_interp_policy  {
	sort `thing'
	gen `thing'_rank = _n
}

rename (*_interp_*) (*_interp_*_x)

graph matrix *_rank
graph export results/exploratory/matrix_plot_of_multi_approaches.png, replace width(1000)

rename (*_interp_*_x) (*_interp_*) 

save data/SSNAP_final.dta, replace
